import ldap3
from django_python3_ldap.conf import settings as ldap_settings


def ldap_connection(user_dn: str = None, password: str = None):
    # Configure the connection.
    if ldap_settings.LDAP_AUTH_USE_TLS:
        auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
    else:
        auto_bind = ldap3.AUTO_BIND_NO_TLS

    ldap_connection = ldap3.Connection(
        ldap3.Server(
            ldap_settings.LDAP_AUTH_URL,
            allowed_referral_hosts=[("*", True)],
            get_info=ldap3.NONE,
            connect_timeout=ldap_settings.LDAP_AUTH_CONNECT_TIMEOUT,
        ),
        user=user_dn if user_dn else ldap_settings.LDAP_AUTH_CONNECTION_USERNAME,
        password=password if password else ldap_settings.LDAP_AUTH_CONNECTION_PASSWORD,
        auto_bind=auto_bind,
        raise_exceptions=True,
        receive_timeout=ldap_settings.LDAP_AUTH_RECEIVE_TIMEOUT,
    )

    return ldap_connection
