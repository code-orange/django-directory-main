import secrets
import string

from django.conf import settings
from ldap3.extend.microsoft.modifyPassword import ad_modify_password

from django_directory_main.django_directory_main.ldap_connection import ldap_connection
from django_directory_main.django_directory_main.lookup import lookup_tenant_user
from django_pwmgr_client.django_pwmgr_client.func import pwmgr_set_single_credentials


def generate_password(length: int = 20):
    alphabet = string.ascii_letters + string.digits

    password = "".join(secrets.choice(alphabet) for i in range(length - 1))
    password += "!"

    return password


def check_credentials(user_dn: str, password: str):
    try:
        ldap_con = ldap_connection(user_dn, password)
    except:
        return False

    if ldap_con:
        return True

    return False


def change_user_password(
    ldap_connection, org_tag: str, key: str, new_password: str, old_password: str = None
):
    user = lookup_tenant_user(
        ldap_connection,
        org_tag=org_tag,
        key=key,
    )

    if not user:
        return False

    if old_password:
        if not check_credentials(user["distinguishedName"][0], old_password):
            return False

    # check cred_id
    cred_id = None

    if "codeorange-pwmgr-id" in user:
        cred_id = user["codeorange-pwmgr-id"][0]

    user_description_template = """
        Display Name: {display_name}
        Distinguished Name (DN): {distinguished_name}
        Common Name (CN): {common_name}
        User Principal Name (UPN): {user_principal_name}
        Mail: {mail}
        """

    user_description = user_description_template.format(
        display_name=user["displayName"][0],
        distinguished_name=user["distinguishedName"][0],
        common_name=user["cn"][0],
        user_principal_name=user["userPrincipalName"][0],
        mail=user["mail"][0] if "mail" in user else "none",
    )

    cred_title = "{} - {}".format(
        user["displayName"][0],
        user["userPrincipalName"][0],
    )

    if len(cred_title) > 64:
        cred_title = user["userPrincipalName"][0]

        if len(cred_title) > 64:
            cred_title = cred_title[0:63]

    pw_change_response = ad_modify_password(
        ldap_connection,
        user["distinguishedName"][0],
        new_password,
        None,
    )

    if pw_change_response:
        pwmgr_response = pwmgr_set_single_credentials(
            settings.MDAT_ROOT_CUSTOMER_ID,
            org_tag=org_tag,
            folder="SRVFARM",
            cred_data={
                "name": cred_title,
                "uri": "https://sso.srvfarm.net/",
                "username": user["sAMAccountName"][0],
                "description": user_description,
                "password_cleartext": new_password,
            },
            cred_id=cred_id,
        )

        if not pwmgr_response:
            pw_second_change_response = ad_modify_password(
                ldap_connection,
                user["distinguishedName"][0],
                generate_password(),
                None,
            )

        return True

    return False
