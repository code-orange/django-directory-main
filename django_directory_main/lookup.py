import copy

import ldap3
from django.conf import settings
from ldap3.utils.conv import escape_bytes

from django_directory_main.django_directory_main.static import DirectoryBackendStatic
from ldap_essential_helpers.ldap_essential_helpers.guid import *
from ldap_essential_helpers.ldap_essential_helpers.san import *
from ldap_essential_helpers.ldap_essential_helpers.sid import *
from ldap_essential_helpers.ldap_essential_helpers.upn import *


def cleanup_object_attributes(attributes):
    attributes_clean = copy.deepcopy(attributes)

    for attribute, values in attributes.items():
        for row_id, row_data in enumerate(values):
            if isinstance(row_data, (bytes, bytearray)):
                attributes_clean[attribute][row_id] = row_data.hex()

    if "objectSid" in attributes_clean:
        attributes_clean["X-SID"] = sid_to_str(
            bytes.fromhex(attributes_clean["objectSid"][0])
        )

    if "objectGuid" in attributes_clean:
        attributes_clean["X-GUID"] = unpack_guid(
            bytes.fromhex(attributes_clean["objectGuid"][0])
        )

    return dict(attributes_clean)


def lookup_ldap_search(ldap_connection, org_tag: str, search_filter: str):
    if not ldap_connection.search(
        search_base="ou=" + org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter=search_filter,
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        return False

    ldap_objects = ldap_connection.response

    if len(ldap_objects) <= 0:
        return False

    return list(ldap_objects)


def lookup_tenant_objects_by_object_class(
    ldap_connection, object_type: str, org_tag: str
):
    # search filter
    if object_type not in DirectoryBackendStatic.ldap_default_search_filters:
        raise KeyError

    search_filter = DirectoryBackendStatic.ldap_default_search_filters[object_type]

    ldap_objects = lookup_ldap_search(
        ldap_connection,
        org_tag=org_tag,
        search_filter=search_filter,
    )

    if not ldap_objects:
        return False

    objects_cleaned = list()

    for ldap_object in ldap_objects:
        objects_cleaned.append(cleanup_object_attributes(ldap_object["attributes"]))

    return objects_cleaned


def lookup_tenant_object_by_object_class_and_key_and_type(
    ldap_connection, object_type: str, org_tag: str, key_type: str, key: str
):
    if key_type == "sid":
        if not is_valid_sid(key):
            return False

        search_attribute = "objectSID"

        key = sid_str_to_hex(key)
    elif key_type == "guid":
        if not is_valid_guid(key):
            return False

        search_attribute = "objectGUID"

        key = escape_bytes(pack_guid(key))
    elif key_type == "upn":
        if not is_valid_upn(key):
            return False

        search_attribute = "userPrincipalName"

    elif key_type == "san":
        if not is_valid_sam_account_name(key):
            return False

        search_attribute = "sAMAccountName"
    else:
        raise KeyError

    # search filter
    if object_type not in DirectoryBackendStatic.ldap_default_search_filters:
        raise KeyError

    search_filter = "(&{base_filter}({search_attribute}={key}))".format(
        base_filter=DirectoryBackendStatic.ldap_default_search_filters[object_type],
        search_attribute=search_attribute,
        key=key,
    )

    ldap_objects = lookup_ldap_search(
        ldap_connection,
        org_tag=org_tag,
        search_filter=search_filter,
    )

    if not ldap_objects:
        return False

    attributes_clean = cleanup_object_attributes(ldap_objects[0]["attributes"])

    return attributes_clean


def lookup_tenant_object_by_object_class_and_key(
    ldap_connection, object_type: str, org_tag: str, key: str
):
    for key_type in ("sid", "guid", "upn", "san"):
        result = lookup_tenant_object_by_object_class_and_key_and_type(
            ldap_connection=ldap_connection,
            object_type=object_type,
            org_tag=org_tag,
            key_type=key_type,
            key=key,
        )

        if result:
            return result

    return False


def cleanup_schema(ldap_object):
    return {
        "name": ldap_object["attributes"]["lDAPDisplayName"][0],
        "dn": ldap_object["dn"],
        "attributes": ldap_object["attributes"],
    }


def lookup_schema(ldap_connection, object_class: str):
    if object_class not in ("attributeSchema", "classSchema"):
        return False

    if not ldap_connection.search(
        search_base=settings.LDAP_SCHEMA_DN,
        search_filter=f"(objectClass={object_class})",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        return False

    ldap_objects = ldap_connection.response

    schema_objects = list()

    for schema_object in ldap_objects:
        schema_objects.append(cleanup_schema(schema_object))

    return schema_objects


def lookup_attribute_schema(ldap_connection):
    return lookup_schema(ldap_connection, "attributeSchema")


def lookup_class_schema(ldap_connection):
    return lookup_schema(ldap_connection, "classSchema")


def lookup_tenant_users(ldap_connection, org_tag: str):
    return lookup_tenant_objects_by_object_class(
        ldap_connection, object_type="user", org_tag=org_tag
    )


def lookup_tenant_user(ldap_connection, org_tag: str, key: str):
    return lookup_tenant_object_by_object_class_and_key(
        ldap_connection, object_type="user", org_tag=org_tag, key=key
    )


def lookup_tenant_groups(ldap_connection, org_tag: str):
    return lookup_tenant_objects_by_object_class(
        ldap_connection, object_type="group", org_tag=org_tag
    )


def lookup_tenant_group(ldap_connection, org_tag: str, key: str):
    return lookup_tenant_object_by_object_class_and_key(
        ldap_connection, object_type="group", org_tag=org_tag, key=key
    )


def lookup_tenant_computers(ldap_connection, org_tag: str):
    return lookup_tenant_objects_by_object_class(
        ldap_connection, object_type="computer", org_tag=org_tag
    )


def lookup_tenant_computer(ldap_connection, org_tag: str, key: str):
    return lookup_tenant_object_by_object_class_and_key(
        ldap_connection, object_type="computer", org_tag=org_tag, key=key
    )


def lookup_tenant_contacts(ldap_connection, org_tag: str):
    return lookup_tenant_objects_by_object_class(
        ldap_connection, object_type="contact", org_tag=org_tag
    )


def lookup_tenant_contact(ldap_connection, org_tag: str, key: str):
    return lookup_tenant_object_by_object_class_and_key(
        ldap_connection, object_type="contact", org_tag=org_tag, key=key
    )
