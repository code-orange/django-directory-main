class DirectoryBackendStatic:
    computer_types = {
        "client": "Workstation",
        "server_windows": "Windows Server",
        "server_linux": "Linux Server",
        "server_nas": "NAS",
    }

    computer_countries = {
        "de": "Germany",
    }

    ldap_default_search_filters = {
        "user": "(&(objectClass=person)(objectClass=user)(!(objectClass=computer)))",
        "group": "(objectClass=group)",
        "computer": "(&(objectClass=person)(objectClass=user)(objectClass=computer))",
        "contact": "(&(objectClass=person)(objectClass=contact)(!(objectClass=computer)))",
    }

    group_types = {
        2: "distribution",
        4: "distribution",
        8: "distribution",
        -2147483646: "security",
        -2147483644: "security",
        -2147483640: "security",
    }
